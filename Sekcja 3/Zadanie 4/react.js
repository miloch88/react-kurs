class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    };
    this.handleClick = this.handleClick.bind(this);
  }

  // state = {
  //   text: ""
  // }

  handleClick() {
    // this.state.text += "A";
    // console.log(this.state.text);

    const number = Math.floor(Math.random() * 10);

    // this.setState({
    //   text: this.state.text + letter
    // });

    this.setState(() => ({
      text: this.state.text + number
    }));
  }

  render() {
    // btnName = "stwórz liczbę"
    return (
      <React.Fragment>
        <button onClick={this.handleClick}>{this.props.btnTitle}</button>
        {/* <PanelResult text={this.state.text}>BBB</PanelResult> */}
        <PanelResult text={this.state.text} />
      </React.Fragment>
    );
  }
}

const PanelResult = props => {
  // return <h1>{props.text}: {props.children}</h1>;
  return (
    <h1>
      {props.text}
    </h1>
  );
};

ReactDOM.render(<App btnTitle="dodaj cyfrę"/>, document.getElementById("root"));
