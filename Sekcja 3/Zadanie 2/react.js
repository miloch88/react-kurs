const Header = () => {
  return <h1>Witaj na mojej stronie</h1>;
};

class Blog extends React.Component {
  // state = {
  //   number: 0
  // };

  render() {
    return (
      <section>
        <h2>Artykuł</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae,
          laborum eos. Nesciunt praesentium debitis corrupti distinctio
          perferendis dolor incidunt nostrum quasi id numquam vitae
          exercitationem laborum, officiis ipsum provident repellat.
        </p>
      </section>
    );
  }
}

const App = () => {
  return (
    <React.Fragment>
      <Header />
      <Blog />
    </React.Fragment>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
