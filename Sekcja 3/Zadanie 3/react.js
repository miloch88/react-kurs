class ShoppingList extends React.Component {
  state = {
    items1: "ogóki",
    items2: "sok",
    items3: "piwo"
  };

  render() {
    return (
      <React.Fragment>
        <h1>Lista zakupów</h1>
        <ul>
          <ItemsList name={this.state.items1} example={2+2}/>
          <ItemsList name={this.state.items2}/>
          <ItemsList name={this.state.items3}/>
        </ul>
      </React.Fragment>
    );
  }
}

// const ItemsList = (props) => {
//   return(
//     <li>{props.name} {props.example}</li>
//   )
// };

class ItemsList extends React.Component{
  render(){
    return(
      <li>{this.props.name} {this.props.example}</li>
    )
  }
}

ReactDOM.render(<ShoppingList />, document.getElementById("root"));
