class Counter extends React.Component {
  state = {
    count: 0,
    result: this.props.result
  };

  handleMatchClick = (type = "reset", number = 1) => {
    // debugger;
    if (type === "substraction") {
      this.setState(prevState => ({
        count: prevState.count + 1,
        result: prevState.result - number
      }));
    } else if (type === "reset") {
      this.setState(prevState => ({
        count: prevState.count + 1,
        result: this.props.result
      }));
    } else if (type === "add") {
      this.setState(prevState => ({
        count: prevState.count + 1,
        result: prevState.result + number
      }));
    }
  };

  render() {
    return (
      <React.Fragment>
        {/* <button onClick={this.handleMatchClick.bind(this, "substraction", 5)}>-5</button>
        <button onClick={()=> this.handleMatchClick("substraction")}>-1</button>
        <button onClick={this.handleMatchClick.bind(this, "reset")}>Reset</button>
        <button onClick={()=> this.handleMatchClick("add")}>+1</button>
        <button onClick={()=> this.handleMatchClick("add", 5)}>+5</button> */}

        <MathButton
          name="-5"
          number={5}
          type="substraction"
          click={this.handleMatchClick}
        />
        <MathButton
          name="-1"
          number={1}
          type="substraction"
          click={this.handleMatchClick}
        />
        <MathButton
          name="Reset"
          number=""
          type="reset"
          click={this.handleMatchClick}
        />
        <MathButton
          name="+1"
          number={1}
          type="add"
          click={this.handleMatchClick}
        />
        <MathButton
          name="+5"
          number={5}
          type="add"
          click={this.handleMatchClick}
        />

        {/* <h1>Liczba kliknięć: {this.state.count}</h1>
        <h1>Wynik: {this.state.result}</h1> */}

        <ResultPanel count={this.state.count} result={this.state.result}/>
      </React.Fragment>
    );
  }
}

const MathButton = props => (
  <button onClick={() => props.click(props.type, props.number)}>
    {props.name}
  </button>
);

const ResultPanel = (props) => {
  return (
    <React.Fragment>
      <h1>Liczba kliknięć: {props.count} {props.count>10 ? <span>UWAGA!</span>: null}</h1>
      <h1>Wynik: {props.result}</h1>
    </React.Fragment>
  );
};

const startValue = 10;
ReactDOM.render(
  <Counter result={startValue} />,
  document.getElementById("root")
);
