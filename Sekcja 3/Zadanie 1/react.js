const header = <h1 className="title">Witaj na stronie</h1>;

const classBig = "big";
const handleClick = () => alert("klick");

const main = (
  <div>
    <h1 onClick={handleClick} className="red">
      Pierwszy artykuł
    </h1>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dicta mollitia
      ex reiciendis? Provident assumenda, suscipit incidunt obcaecati nulla
      deleniti omnis voluptates ad ullam quas, ab nihil ea dolore beatae minima!
    </p>
  </div>
);

const text = "stopkaaaa";
const footer = (
  <footer>
    <p className={classBig}>{text}</p>
  </footer>
);

const app = [header, main, footer];

ReactDOM.render(app, document.getElementById("root"));
