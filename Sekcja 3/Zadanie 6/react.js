class Message extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      messageIsActive: false
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (){
    this.setState({
      messageIsActive: !this.state.messageIsActive
    })
  }
  
  render() {

    console.log(this.state.messageIsActive);

    const text = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni impedit possimus nostrum corrupti. Quaerat perferendis aliquid labore, asperiores velit quasi, impedit molestiae non voluptates quia alias qui. Quis, ex reprehenderit.'

    return (

      
      <React.Fragment>
        <button onClick={this.handleClick}>{this.state.messageIsActive ? "Ukryj": "Pokaż"}</button>
        
        <p>{this.state.messageIsActive && text}</p>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Message />, document.getElementById("root"));
