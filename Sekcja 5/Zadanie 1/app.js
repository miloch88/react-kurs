const data = {
  omens: [
    { id: 0, text: "Pierwsza wróżba" },
    { id: 1, text: "Druga wróżba" },
    { id: 2, text: "Trzecia wróżba" }
  ]
};

const Text = props => <div>Twoja wróżba to: {props.text}</div>;

class Omen extends React.Component {
  state = {
    number: "",
    newOmen: ""
  };

  handleRandom = () => {
    this.setState({
      number: Math.floor(Math.random() * this.props.data.omens.length)
    });
  };

  handleInsert = e => {
    this.setState({
      newOmen: e.target.value
    });
  };

  handleAddOmen = () => {
    if (this.state.newOmen === "") return alert("Wpisz jakąś wróźbę!");
    else {
      this.props.data.omens.push({
        id: this.props.data.omens.length,
        text: this.state.newOmen
      });
      this.setState({
        newOmen: ""
      });

      let textalert = "";
      this.props.data.omens.forEach((element, i) => {
        if (i === this.props.data.omens.length - 1) {
          textalert += `${element.text} `;
        } else {
          textalert += `${element.text}, `;
        }
      });
      alert(textalert);
    }
  };

  getOmen = id => {
    let omens = this.props.data.omens.filter(omen => omen.id === id);
    return omens.map(omen => <Text key={omen.id} text={omen.text} />);
  };

  render() {
    const { number, newOmen } = this.state;
    return (
      <React.Fragment>
        <button onClick={this.handleRandom}>Losuj wróżbę</button>
        <br />
        <label>
          <input
            name="newOmen"
            value={newOmen}
            type="text"
            onChange={this.handleInsert}
          />
          <button onClick={this.handleAddOmen}>Dodaj nową wrożbę</button>
        </label>
        {this.getOmen(number)}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Omen data={data} />, document.getElementById("root"));
