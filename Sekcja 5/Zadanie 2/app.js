const Person = props => (
  <li>
    {props.name}
    {/* <button onClick={props.delete}>Usuń</button> */}
    <button onClick={() => props.delete(props.id)}>Usuń</button>
  </li>
);

class List extends React.Component {
  state = {
    people: [
      { id: 0, name: "Jace Beleren" },
      { id: 1, name: "Chandra Nalaar" },
      { id: 2, name: "Liliana Vess" },
      { id: 3, name: "Nissa Revane" }
    ]
  };

  handleDelete = id => {
    const people = [...this.state.people];
    const index = people.findIndex(person => person.id === id);
    people.splice(index, 1);
    this.setState({
      people
    });
  }

  // handleDelete(name){
  //   let people = this.state.people.slice()
  //   people = people.filter(person => name != person.name)
  //   this.setState({
  //     people
  //   })
  // }

  render() {
    const personsList = this.state.people.map(person => (
      <Person
        key={person.id}
        name={person.name}
        // delete={this.handleDelete.bind(this, person.id)}
        delete={this.handleDelete}
      />
    ));

    return (
      <React.Fragment>
        <ul>{personsList}</ul>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<List />, document.getElementById("root"));
