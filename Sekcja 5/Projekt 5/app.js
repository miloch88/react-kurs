class Form extends React.Component {
  state = {
    city: "Londyn",
    text: "",
    isLike: true,
    number: "2"
  };

  handleChange = e => {
    if (e.target.type === "checkbox") {
      this.setState({
        [e.target.name]: e.target.checked
      });
    } else {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
  };

  render() {
    return (
      <React.Fragment>
        <label>
          Podaj miasto:
          <input
            name="city"
            value={this.state.city}
            onChange={this.handleChange}
            type="text"
          />
        </label>
        <br />
        <label>
          Napisz coś o tym mieście:
          <textarea
            name="text"
            value={this.state.text}
            onChange={this.handleChange}
          />
        </label>
        <br />
        <label>
          Czy lubisz to miasto:
          <input
            name="isLike"
            checked={this.state.isLike}
            onChange={this.handleChange}
            type="checkbox"
          />
        </label>
        <br />
        <label>
          Ile razy tam byłeś?
          <select
            name="number"
            value={this.state.number}
            onChange={this.handleChange}
          >
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="more">Więcej</option>
          </select>
        </label>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Form />, document.getElementById("root"));
