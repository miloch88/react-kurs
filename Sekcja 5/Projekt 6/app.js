const Cash = props => (
  <div>
    {props.title}{" "}
    {props.cash <= 0
      ? ""
      : ((props.cash / props.ratio) * props.price).toFixed(2)}
  </div>
);

class ExchangeCounter extends React.Component {
  state = {
    amount: "",
    product: "gas"
  };

  static defaultProps = {
    currencies: [
      { id: 0, name: "zloty", ratio: 1, title: "Wartość w złotówkach: " },
      { id: 1, name: "dolar", ratio: 3.6, title: "Wartość w dolarach: " },
      { id: 2, name: "euro", ratio: 4.4, title: "Wartość w euro: " },
      { id: 3, name: "funt", ratio: 4.8, title: "Wartość w funtach: " }
    ],
    prices: {
      electricity: 0.51,
      gas: 5.12,
      oranges: 3.45
    }
  };

  handleChange = e => {
    this.setState({
      amount: e.target.value
    });
  };

  handleSelect = e => {
    this.setState({
      product: e.target.value,
      amount: ""
    });
  };

  insertSuffix(select) {
    if (select === "electricity") return <em> kWh</em>;
    else if (select === "gas") return <em> litrów</em>;
    else if (select === "oranges") return <em> kilogramów</em>;
    else return null;
  }

  selectPrice(select) {
    return this.props.prices[select];
  }

  render() {
    const { amount, product } = this.state;
    const price = this.selectPrice(product);

    const calculators = this.props.currencies.map(currency => (
      <Cash
        key={currency.id}
        title={currency.title}
        ratio={currency.ratio}
        cash={amount}
        price={price}
      />
    ));

    return (
      <React.Fragment>
        <div className="app">
          <label>
            Wybierz produkt:
            <select value={product} onChange={this.handleSelect}>
              <option value="electricity">prąd</option>
              <option value="gas">benzyna</option>
              <option value="oranges">pomarańcze</option>
            </select>
            <br />
          </label>
          <label>
            <input type="number" value={amount} onChange={this.handleChange} />
            {this.insertSuffix(product)}
          </label>
          {calculators}
        </div>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<ExchangeCounter />, document.getElementById("root"));
