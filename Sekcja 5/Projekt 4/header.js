const Header = (props) => {
  const activeItmes = props.items.filter(item => item.active);
  const number = activeItmes.length;
  return (
    <header>
      <h2>Wielkośc zamówienia: {number}</h2>
      <h2>Do zapłaty: {number ? `${number * 10} złotych`: `Jeszcze nic nie wybrałeś` }</h2>
    </header>
  );
};
