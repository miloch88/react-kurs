import React from "react";
import "./Word.css";

const Word = props => (
  <li key="props.key">
    Słowo po angielsku: <strong>{props.english}</strong>. <br />
    Tłumacznie: <strong>{props.polish}</strong>
  </li>
);

export default Word;
