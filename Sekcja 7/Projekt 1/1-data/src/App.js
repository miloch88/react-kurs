import React, { Component } from "react";
import "./App.css";

const data = [
  { id: 1, title: "Wiadomosć numer 1", body: "Zawartość wiadomości 1..." },
  { id: 2, title: "Wiadomosć numer 2", body: "Zawartość wiadomości 2..." }
];

setInterval(() => {
  const index = data.length + 1;
  data.push({
    id: index,
    title: `Wiadomosć numer ${index}`,
    body: `Zawartość wiadomości ${index}...`
  });
}, 8000);

class App extends Component {
  state = {
    comments: [...data]
  };

  getData = () => {
    console.log("aktualizacja");
    if (this.state.comments.length !== data.length) {
      
      this.setState({
        comments: [...data]
      });
    }else{
      console.log('Dane takie same')
    }
  };

  componentDidMount() {
    this.idI = setInterval(this.getData, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.idI);
  }

  render() {
    const comments = this.state.comments.map(comment => (
      <div key={comment.id}>
        <h4>{comment.title}</h4>
        <div>{comment.body}</div>
      </div>
    ));
    return <div className="App">{comments}</div>;
  }
}

export default App;
