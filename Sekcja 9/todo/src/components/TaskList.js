import React from "react";
import Task from "./Task";

const TaskList = props => {
  const active = props.tasks.filter(task => task.active);
  const done = props.tasks.filter(task => !task.active);

  active.sort((a, b) => {
    a = a.text.toLowerCase();
    b = b.text.toLowerCase();
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  });
  done.sort((a, b) => b.finishDate - a.finishDate);

  const activeTasks = active.map(task => (
    <Task
      key={task.id}
      task={task}
      delete={props.delete}
      change={props.change}
    />
  ));

  const doneTasks = done.map(task => (
    <Task
      key={task.id}
      task={task}
      delete={props.delete}
      change={props.change}
    />
  ));

  return (
    <div>
      <div className="active">
        <h1>Zadania do zrobienia </h1>
        {activeTasks.length > 0 ? activeTasks : "Brak zadań!!!"}
      </div>
      <hr />
      <div className="done">
        <h3>
          Zadania zrobione <em>({doneTasks.length})</em>
        </h3>
        {done.length > 5 && (
          <span style={{ fontSize: "13px" }}>
            Wyświetlanych jest jedynie 5 ostatnich zadań
          </span>
        )}
        {doneTasks.slice(0, 5)}
      </div>
    </div>
  );
};

export default TaskList;
